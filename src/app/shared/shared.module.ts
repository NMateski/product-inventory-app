import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticatedLayoutComponent, NavbarComponent, PublicLayoutComponent } from './components';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ConfirmationPopupComponent } from './components/confirmation-popup/confirmation-popup.component';
import { InterceptorInterceptor } from './interceptor/interceptor.interceptor';

const COMPONENTS = [
    NavbarComponent,
    AuthenticatedLayoutComponent,
    PublicLayoutComponent,
    ConfirmationPopupComponent
];

const MODULES = [
  CommonModule,
  RouterModule,
  HttpClientModule,
  ReactiveFormsModule
];

@NgModule({
  declarations: [
    ...COMPONENTS
  ],
  imports: [
    ...MODULES,
    ModalModule.forChild()
  ],
  exports: [
    ...COMPONENTS,
    ...MODULES
  ],
  entryComponents: [
    ConfirmationPopupComponent
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: InterceptorInterceptor, multi: true }],
})
export class SharedModule { }

import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-confirmation-popup',
  templateUrl: './confirmation-popup.component.html',
  styleUrls: ['./confirmation-popup.component.scss']
})
export class ConfirmationPopupComponent implements OnInit {

  public confirmed = false;
 
  constructor(public bsModalRef: BsModalRef) {}
 
  ngOnInit() {
  }

  public onYes(): void {
    this.confirmed = true;
    this.bsModalRef.hide();
  }

  public onNo(): void {
    this.confirmed = false;
    this.bsModalRef.hide();
  }


}

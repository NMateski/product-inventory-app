import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Inventory, InventoryRequest } from './inventories.domain';
import { catchError, map } from 'rxjs/operators';
import { Page } from '../shared';

const API_ENDPOINT = 'inventories';
const HEADERS = new HttpHeaders({
  Accept: 'application/json'
});

@Injectable()
export class InventoryService {

  constructor(private httpClient: HttpClient) { }

  public getPage(page: number, size: number): Observable<Inventory[]> {
    return this.httpClient.get<any>(`${environment.apiUrl}/${API_ENDPOINT}?page=${page}&size=${size}`, {headers: HEADERS}).pipe(
      map( (response) => (response.content as any[]).map((item) => new Inventory(item.id, item.name, item.location))),
      catchError((error) => {
        console.error(error);
        return of([]);
      })
    );
  }

  public getPageV2(page: number, size: number): Observable<Page<Inventory>> {
    const params = new HttpParams()
      .append('page', page)
      .append('size', size);

    return this.httpClient.get<Page<Inventory>>(`${environment.apiUrl}/${API_ENDPOINT}`, {headers: HEADERS, params: params}).pipe(
      catchError((error) => {
        console.error(error);
        return of(new Page<Inventory>([], 0, 0));
      })
    );
  }

  public create(request: InventoryRequest): Observable<Inventory> {
    return this.httpClient.post<Inventory>(`${environment.apiUrl}/${API_ENDPOINT}`, request, {headers: HEADERS}).pipe(
      catchError((error) => {
        console.error(error);
        return throwError(error);
      })
    );
  }

  public getById(id: number): Observable<Inventory> {
    return this.httpClient.get<Inventory>(`${environment.apiUrl}/${API_ENDPOINT}/${id}`, {headers: HEADERS}).pipe(
      catchError((error) => {
        console.error(error);
        return throwError(error);
      })
    );
  }

  public update(id: number, request: InventoryRequest): Observable<Inventory> {
    return this.httpClient.put<Inventory>(`${environment.apiUrl}/${API_ENDPOINT}/${id}`, request, {headers: HEADERS}).pipe(
      catchError((error) => {
        console.error(error);
        return throwError(error);
      })
    );
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(`${environment.apiUrl}/${API_ENDPOINT}/${id}`, {headers: HEADERS}).pipe(
      catchError((error) => {
        console.error(error);
        return throwError(error);
      })
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Page } from 'src/app/shared';
import { ProductService } from '../product.service';
import { ProductRequest } from '../products.domain';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  formGroup: FormGroup;

  constructor(private router: Router, private productService: ProductService, private route: ActivatedRoute) { 
    this.formGroup = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        Validators.maxLength(150)
      ]),
      type: new FormControl(null, [
        Validators.required,
        Validators.maxLength(50)
      ]),
      price: new FormControl(null, [
        Validators.required
      ]),
      description: new FormControl(null, [
        Validators.maxLength(1000)
      ])
    });
  }

  ngOnInit(): void {
  }

  public onSubmit(): void {
    if(this.formGroup.invalid) {
      console.log('Form is not valid!');
      console.log(this.formGroup.errors);
      return;
    }

    const name = this.formGroup.value.name;
    const type = this.formGroup.value.type;
    const price = this.formGroup.value.price;
    const description = this.formGroup.value.description;

    const inventoryId = this.route.snapshot.paramMap.get('inventoryId');

    if (!inventoryId) {
      console.log('Inventory ID is required!!!');
      return;
    }

    this.productService.create(Number(inventoryId), new ProductRequest(name, type, price, description)).subscribe({
      next: product => this.router.navigate([`../${product.id}`], {relativeTo: this.route})
    });
  }

}

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Page } from 'src/app/shared';
import { environment } from 'src/environments/environment';
import { Product, ProductRequest } from './products.domain';

const API_ENDPOINT = 'products';
const HEADERS = new HttpHeaders({
  Accept: 'application/json'
});

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient: HttpClient) { }

  public getPage(inventoryId: number, page: number, size: number): Observable<Page<Product>> {
    const params = new HttpParams()
      .append('page', page)
      .append('size', size)
      .append('inventoryId', inventoryId);

    return this.httpClient.get<Page<Product>>(`${environment.apiUrl}/${API_ENDPOINT}`, {headers: HEADERS, params: params}).pipe(
      catchError((error) => {
        console.error(error);
        return of(new Page<Product>([], 0, 0));
      })
    );
  }

  public create(inventoryId: number, request: ProductRequest): Observable<Product> {
    const params = new HttpParams()
      .append('inventoryId', inventoryId);

    return this.httpClient.post<Product>(`${environment.apiUrl}/${API_ENDPOINT}`, request, {headers: HEADERS, params: params}).pipe(
      catchError((error) => {
        console.error(error);
        return throwError(error);
      })
    );
  }

  public update(id: number, request: ProductRequest): Observable<Product> {
    return this.httpClient.put<Product>(`${environment.apiUrl}/${API_ENDPOINT}/${id}`, request, {headers: HEADERS}).pipe(
      catchError((error) => {
        console.error(error);
        return throwError(error);
      })
    );
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(`${environment.apiUrl}/${API_ENDPOINT}/${id}`, {headers: HEADERS}).pipe(
      catchError((error) => {
        console.error(error);
        return throwError(error);
      })
    );
  }

  public getById(id: number): Observable<Product> {
    return this.httpClient.get<Product>(`${environment.apiUrl}/${API_ENDPOINT}/${id}`, {headers: HEADERS}).pipe(
      catchError((error) => {
        console.error(error);
        return throwError(error);
      })
    );
  }
}

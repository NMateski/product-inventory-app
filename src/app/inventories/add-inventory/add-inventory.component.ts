import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { InventoryRequest } from '../inventories.domain';
import { InventoryService } from '../inventory.service';

@Component({
  selector: 'app-add-inventory',
  templateUrl: './add-inventory.component.html',
  styleUrls: ['./add-inventory.component.scss']
})
export class AddInventoryComponent implements OnInit {

  formGroup: FormGroup;

  constructor(private router: Router, private inventoryService: InventoryService) { 
    this.formGroup = new FormGroup({
      name: new FormControl(null, [
        Validators.maxLength(100)
      ]),
      location: new FormControl(null, [
        Validators.maxLength(50)
      ])
    });
  }

  ngOnInit(): void {
  }

  public onSubmit(): void {
    if(this.formGroup.invalid) {
      console.log('Form is not valid!');
      console.log(this.formGroup.errors);
      return;
    }

    const name = this.formGroup.value.name;
    const location = this.formGroup.value.location;

    this.inventoryService.create(new InventoryRequest(name, location)).subscribe({
      next: inventory => this.router.navigateByUrl(`/inventories/${inventory.id}`)
    });

    // console.log(`Name: ${name}, Location: ${location}`);
  }

}
